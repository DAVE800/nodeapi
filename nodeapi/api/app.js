var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')
var fileupload = require("express-fileupload")
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var uploads = require('./routes/shapes');
var clothsroute=require('./routes/cloths');
var partners = require('./routes/partners');
var gadgets = require('./routes/gadgets');
var packs = require('./routes/packs');
var displays = require('./routes/displays');
var printed = require('./routes/printed');
var tops=require('./routes/tops');
var orders=require('./routes/orders');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileupload());
app.use('/',indexRouter);
app.use('/users', usersRouter);
app.use('/shapes',uploads);
app.use('/cloths',clothsroute);
app.use('/partners',partners);
app.use('/gadgets', gadgets);
app.use('/packs', packs);
app.use('/disps', displays);
app.use('/prints',printed);
app.use('/tops',tops);
app.use('/orders',orders);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
