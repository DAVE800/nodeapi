var express = require('express');
var router = express.Router();
var db = require('../config/db')

/* Post uploaded file.
 */

router.post('/', function(req, res, next){
    const file = req.files.link
    file.mv("public/images/"+file.name,(err,res)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_shapes(link, size) VALUES ?";
            var value=[
                ["https://api.aladin.ci/images/"+file.name,file.size]
            ];
            db.query(sql,[value],(err,res)=>{
                if(err){
                    throw err
                }else{
                console.log(res);
                }
            });
       }
    });

});

/**
 * get all uploaded shapes
 */
router.get('/',(req,res,next)=>{
 let sql = "SELECT * FROM aladin_shapes"
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{
    res.send({status:200,shapes:result});
}
})
});

router.get("/:id",(req,res)=>{
let sql = `SELECT * FROM aladin_shapes WHERE shap_id=${req.params.id}`;
db.query(sql,(err,result)=>{
    if(err) throw err
    console.log(result);
    let r={
        status:200,
        shape:result
    }
    res.send(r);

});

});




module.exports = router;
