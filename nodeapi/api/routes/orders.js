    var express = require('express');
    var db = require('../config/db');
    const sendmail=require('../config/mail/notify')
    const order_date=require('../config/order_date')
    var router =express.Router();


/**
 * get all pending orders
*/
router.get('/pending',(req,res)=>{
    var sql =`SELECT * FROM aladin_orders WHERE status=${'pending'}`;
    db.query(sql,(err,resul)=>{
        if(err){
            throw err
        }else
        {
            res.send({
                 status:true,
                 data:resul

            }
            );
        }
    });
})


/***
 * get all delivered orders
*/
router.get('/ok',(req,res)=>{
    var sql  =`SELECT * FROM aladin_orders WHERE status=${'livrée'}`;
    db.query(sql,(err,r)=>{
        if (err)throw err;
        res.send(r);
    });
});


router.get('/new',(req,res)=>{
    var sql  =`SELECT * FROM aladin_orders WHERE status=${'new'}`;
    db.query(sql,(err,r)=>{
        if (err)throw err;
        res.send(r);
    });
});


/**
 * 
 * 
*/
router.get('/aladin/orders',(req,res)=>{
    let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.prod_link,aladin_product_ordered.prod_price, aladin_product_ordered.prod_total,aladin_product_ordered.prod_qty FROM aladin_orders
   INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
    INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
    `;
    db.query(sql,(err,result)=>{
        if(err){
            res.send(
                {
                error:err,
                status:false,
                message:"erreur mauvaise requete"
                })
        }
        res.send({
            data:result,
            status:true,
            message:"succès"
            
        }
    )

    })
})

    router.post('/',function(req, res, next){
        var sql = "INSERT INTO aladin_orders(status,customer,created_at,dmode) VALUES ?";
        var user=parseInt(req.body.customer);
        var created_at=order_date();
        var value=[
            [req.body.status,user,created_at,req.body.dmode]
        ];
        db.query(sql,[value],(err,result)=>{
            if(err){
                res.send(err)
            }else{

                db.query(`SELECT * FROM aladin_users WHERE user_id=${result[0].user_id}`,(err,ok)=>{
                if(err){
                    throw err;
                }else{
                    let to =ok[0].email;
                    let html=`Bonjour votre commande numero ${result[0].ord_id}`;
                    let sub="Aladin";
                    sendmail(to,sub,html);
                    res.send({
                        message:"commande ajoutée",
                        resp:result
                    });
                }

                });
    
            }
        });
    });


    router.post('/prods',(req,res,next)=>{
    let data ={
    prod_link: req.body.prod_link,
    prod_price:req.body.prod_price,
    qty:req.body.qty,
    total:req.body.total,
    orders:req.body.orders,
    cloth:req.body.cloth,
    print:req.body.print,
    pack:req.body.pack,
    gadget:req.body.gadget,
    disp:req.body.disp
    };
    var sql=" INSERT INTO aladin_product_ordered(prod_link,prod_price,cloth,print,pack,disp,gadget,orders,total,qty) VALUES ?";
    var values=[
        [data.prod_link,data.prod_price,data.cloth,data.print,data.pack,data.disp,data.gadget,data.orders,data.total,data.qty]
    ];

    db.query(sql,[values],(error,result)=>{
    if(error){
        throw error
    }else{
        if(data.cloth!=null){
            var request=`SELECT * FROM aladin_cloths WHERE clt_id=${data.cloth}`;
            db.query(request,(error,ok)=>{
            if(error){
                throw error
            }else{
                let partner = ok[0].user_id;
                let status="pending"
                let setdata="INSERT INTO aladin_partner_prod_ordered(link,status,qty,created_at,total,partner) VALUES ?";
                let value=[[data.prod_link,status,data.qty,order_date(),data.total,partner]];
                db.query(setdata,[value],(err,rep)=>{
                    if(err) throw err;
                    let part=`SELECT * FROM aladin_users WHERE user_id=${partner} AND is_partner=${1}`;
                    db.query(part,(err,r)=>{
                        if(err) throw err;
                        let email = r[0].email;
                        let sub="ALADIN";
                        let html=`commande`;
    
                        sendmail(email,sub,html);
                        res.send({
                            message:"products saved",
                            data:result
                        });
    
                    });

                });

            
            }
            });

        }


        if(data.disp!=null){
            var request=`SELECT * FROM aladin_displays WHERE disp_id=${data.disp}`;
            db.query(request,(error,ok)=>{
            if(error){
                throw error;
            }else{
                let partner = ok[0].user_id;
                let status="pending"
                let setdata="INSERT INTO aladin_partner_prod_ordered(link,status,qty,created_at,total,partner) VALUES ?";
                let value=[[data.prod_link,status,data.qty,order_date(),data.total,partner]];
                db.query(setdata,[value],(err,rep)=>{
                    if(err) throw err;
                    let part=`SELECT * FROM aladin_users WHERE user_id=${partner} AND is_partner=${1}`;
                    db.query(part,(err,r)=>{
                        if(err) throw err;
                        let email = r[0].email;
                        let sub="ALADIN";
                        let html=`commande`;
    
                        sendmail(email,sub,html);
                        res.send({
                            message:"products saved",
                            data:result
                        });
    
                    });

                });

            }
            });
            
        }

        if(data.gadget!=null){

            var request=`SELECT * FROM aladin_gadgets WHERE gadg_id=${data.gadget}`;
            db.query(request,(error,ok)=>{
            if(error){
                throw error
            }else{
                let partner = ok[0].user_id;
                let status="pending"
                let setdata="INSERT INTO aladin_partner_prod_ordered(link,status,qty,created_at,total,partner) VALUES ?";
                let value=[[data.prod_link,status,data.qty,order_date(),data.total,partner]];
                db.query(setdata,[value],(err,rep)=>{
                    if(err) throw err;
                    let part=`SELECT * FROM aladin_users WHERE user_id=${partner} AND is_partner=${1}`;
                    db.query(part,(err,r)=>{
                        if(err) throw err;
                        let email = r[0].email;
                        let sub="ALADIN";
                        let html=`commande`;
    
                        sendmail(email,sub,html);
                        res.send({
                            message:"products saved",
                            data:result
                        });
    
                    });

                });

            }
            });
        }

        if(data.pack){
            var request=`SELECT * FROM aladin_packaging WHERE pack_id=${data.pack}`;
            db.query(request,(error,ok)=>{
            if(error){
                throw error;
            }else{
                let partner = ok[0].user_id;
                let status="pending"
                let setdata="INSERT INTO aladin_partner_prod_ordered(link,status,qty,created_at,total,partner) VALUES ?";
                let value=[[data.prod_link,status,data.qty,order_date(),data.total,partner]];
                db.query(setdata,[value],(err,rep)=>{
                    if(err) throw err;
                    let part=`SELECT * FROM aladin_users WHERE user_id=${partner} AND is_partner=${1}`;
                    db.query(part,(err,r)=>{
                        if(err) throw err;
                        let email = r[0].email;
                        let sub="ALADIN";
                        let html=`commande`;
    
                        sendmail(email,sub,html);
                        res.send({
                            message:"products saved",
                            data:result
                        });
    
                    });

                });

            }
            });

        }

        if(data.print){
            var request=`SELECT * FROM aladin_printed WHERE print_id=${data.print}`;
            db.query(request,(error,ok)=>{
            if(error){
                throw error
            }else{
                let partner = ok[0].user_id;
                let status="pending"
                let setdata="INSERT INTO aladin_partner_prod_ordered(link,status,qty,created_at,total,partner) VALUES ?";
                let value=[[data.prod_link,status,data.qty,order_date(),data.total,partner]];
                db.query(setdata,[value],(err,rep)=>{
                    if(err) throw err;
                    let part=`SELECT * FROM aladin_users WHERE user_id=${partner} AND is_partner=${1}`;
                    db.query(part,(err,r)=>{
                        if(err) throw err;
                        let email = r[0].email;
                        let sub="ALADIN";
                        let html=`commande`;
                        sendmail(email,sub,html);
                        res.send({
                            message:"products saved",
                            data:result
                        });
    
                    });

                });

            }
            });

        }
    }
    });

    });

    router.get('/customers/:id',(req,res)=>{
        let data=[]
        let user= req.params.id;
        var sql =`SELECT * FROM aladin_orders WHERE customer=${user} `;
        db.query(sql,(err,result)=>{
            if(err) throw err;
          let order=result[0].ord_id;
          var reqt=`SELECT * FROM aladin_product_ordered WHERE orders=${order}`;
          db.query(reqt,(err,resp)=>{
              if(err) throw err;
              data.push({status:result[0].status,created:result[0].created_at,prods:resp});
              res.send(data);
          });
        });    
    });



 router.get('/partners/:id',(req,res)=>{
     var sql =`SELECT * FROM aladin_partner_prod_ordered WHERE partner=${req.params.id}`;
     db.query(sql,(err,rest)=>{
         if(err) throw err;
         res.send(rest);
     });

 });




 router.get('/admin',(req,res)=>{
     var sql =`SELECT aladin_orders.ord_id,aladin_orders.dmode,aladin_partner_prod_ordered.status,aladin_orders.created_at,aladin_users.name,aladin_orders.customer FROM aladin_partner_prod_ordered
     
    JOIN aladin_orders ON aladin_partner_prod_ordered.orders=aladin_orders.ord_id

    JOIN aladin_users ON aladin_users.user_id=aladin_partner_prod_ordered.partner

   `
    db.query(sql,(err,result)=>{
        if(err) throw err;
        console.log(result);
        res.send(result)
    })
 });


router.get('/o',(req,res)=>{
    var sql=`SELECT * FROM aladin_partner_prod_ordered` 
    db.query(sql,(err,r)=>{
        if(err) throw err;
        res.send(r);

    });
})
 module.exports = router;
