var express = require('express');
var router = express.Router();
var db = require('../config/db')
const sendmail=require('../config/mail/notify')

/* Post uploaded file. */

router.put('/:id',(req,res)=>{
    let sql =`UPDATE aladin_cloths SET is_ordered=${req.body.is_ordered} WHERE clt_id=${req.params.id}`
    db.query(sql,(err,result)=>{
      if(err) throw err;
      let newsql=`SELECT user_id FROM aladin_cloths WHERE clt_id=${req.params.id}`;
   
     db.query(newsql,(err,re)=>{
         if(err) throw err;
         let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;
         db.query(ql,(err,r)=>{
             if(err) throw err;
  
             let message={
        to:r[0].email,
        subject:"Activation de compte Aladin",
        html:"Votre produit a été approuvé."
      };
      sendmail(message.to,message.subject,message.html);
      
            res.send({
                message:"ous avez approuvé le produit",
                success:true
            });


         })
     });

  
    })
    
  });
router.post('/', function(req, res, next){
    const file = req.files.img
    console.log(file,req.body);
    file.mv("public/cloths/"+file.name,(err,resp)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_cloths(name_cloths,price,color,mode,size,material,type_imp,img,user_id,comment) VALUES ?";

            var user=parseInt(req.body.user_id);
            console.log(user);

            var value=[
                [req.body.name_cloths,req.body.price,req.body.color,req.body.mode,req.body.size,req.body.material,req.body.type_imp,"https://api.aladin.ci/cloths/"+file.name,user,req.body.comment]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    res.send(err)
                }else{
                res.send({
                    message:"Vetement ajouté",
                    resp:result
                });
                }
            });
       }
    });
  
});

/**
 * get all uploaded cloths
 */
router.get('/',(req,res,next)=>{
sql = `SELECT * FROM aladin_cloths WHERE is_ordered=${1}`
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{

    res.send(result);
}
})
});

router.get('/:id',(req,res)=>{
    if(req.params.id){
        let sql=`SELECT aladin_cloths.clt_id,aladin_cloths.price,aladin_cloths.color,aladin_cloths.mode,aladin_cloths.size,aladin_cloths.material,aladin_cloths.type_imp,aladin_cloths.img,aladin_cloths.comment,aladin_cloths.staff,aladin_cloths.user_id,aladin_cloths.name_cloths,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back FROM aladin_images
        JOIN aladin_cloths ON aladin_images.cloth=aladin_cloths.clt_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_cloths.clt_id=${req.params.id}`;
        db.query(sql,(err,result)=>{
            if(err){
           res.send(err)
            } 
            res.send(result);
        })
    }
})


/**
 * list cloths
 * router.get('/',(req,res)=>{
    let sql =`SELECT `
    clt_id 	name_cloths 	price 	color 	mode 	size 	material 	type_imp 	img 	is_ordered 	cat_id 	created_at 	updated_at 	deleted_at 	user_id 	comment 	staff 
});
 */


module.exports = router;
