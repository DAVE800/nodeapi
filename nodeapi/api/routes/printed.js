var express = require('express');
var router = express.Router();
var db = require('../config/db')
const sendmail=require('../config/mail/notify')

/* Post uploaded file. */

router.post('/', function(req, res, next){
    const file = req.files.img
    console.log(file,req.body);
    file.mv("public/prints/"+file.name,(err,resp)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_printed(name_printed,price,dim,gram,pellicule,border,volet,verni,img,user_id,comment) VALUES ?";
            var user=parseInt(req.body.user_id)
            var value=[
                [req.body.name_printed,req.body.price,req.body.dim,req.body.gram,req.body.pellicule,req.body.border,req.body.volet,req.body.verni,"https://api.aladin.ci/prints/"+file.name,user,req.body.comment]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    throw err
                }else{
                res.send({
                    message:"Vetement ajouté",
                    resp:result
                });
                }
            });
       }
    });
  
});

/**
 * get all uploaded cloths
 */
router.get('/',(req,res,next)=>{
sql = `SELECT * FROM aladin_printed WHERE is_ordered=${1}`
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{

    res.send(result);
}
})
});





router.get('/:id',(req,res)=>{
    if(req.params.id){
        let sql=`SELECT aladin_printed.print_id,aladin_printed.price,aladin_printed.dim,aladin_printed.is_ordered,aladin_printed.staff,aladin_printed.user_id,aladin_printed.comment,aladin_printed.created_at,aladin_printed.name_printed,aladin_printed.gram,aladin_printed.border,aladin_printed.pellicule,aladin_printed.verni,aladin_printed.volet,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back FROM aladin_images
        JOIN aladin_printed ON aladin_images.print=aladin_printed.print_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_printed.print_id=${req.params.id}`;
        db.query(sql,(err,result)=>{
            if(err){
           res.send(err)
            } 
            res.send(result);
        })
    }
});

router.put('/:id',(req,res)=>{
    let sql =`UPDATE aladin_printed SET is_ordered=${req.body.is_ordered} WHERE print_id=${req.params.id}`
    db.query(sql,(err,result)=>{
      if(err) throw err;
      let newsql=`SELECT user_id FROM aladin_printed WHERE print_id=${req.params.id}`;
   
     db.query(newsql,(err,re)=>{
         if(err) throw err;
         let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;
         db.query(ql,(err,r)=>{
             if(err) throw err;
  
             let message={
        to:r[0].email,
        subject:"Activation de compte Aladin",
        html:"Votre produit a été approuvé."
      };
      sendmail(message.to,message.subject,message.html);
      
            res.send({
                message:"ous avez approuvé le produit",
                success:true
            });


         })
     });

  
    })
    
  });


module.exports = router;
