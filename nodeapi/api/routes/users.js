var express = require('express');
var router = express.Router();
var db=require('../config/db');
var sendmail=require('../config/mail/notify');
const bcrypt=require("bcrypt");
var generator = require('generate-password');
var createhtml=require('../config/html');
const order_date=require('../config/order_date');
const createuserhtml = require('../config/userhtml');
const pwdhtml=require('../config/pwd');

/* GET users listing. */

router.get('/', function(req, res, next) {
  db.query(`SELECT * FROM aladin_users WHERE is_partner=${0}`,function(err,result){
    if(err) throw err
    res.send(result);
  });
});


router.put('/:id', function(req, res, next) {
  let sql =`UPDATE aladin_users SET activated=${req.body.activated} WHERE user_id=${req.params.id}`

  db.query(sql,function(err,result){
    if(err) throw err
    let newsql=`SELECT email FROM aladin_users WHERE user_id=${req.params.id}`;
    db.query(newsql,(err,r)=>{
      if(err) throw err;
      let message={
        to:r[0].email,
        subject:"Aladin password",
        html:"Votre compte à été activé vous pouvez dersomaire ajouté vos produits."
      };
      sendmail(message.to,message.subject,message.html);
      res.send({
        success:true,
        message:`vous avez approuvé le compte numero: ${req.params.id} et un email à été envoyé ${r[0].email}`
      });
    });
  });
});


router.get('/part/details',(req,res)=>{

  db.query(`SELECT aladin_users.user_id,aladin_users.name,aladin_users.email,aladin_users.country,aladin_users.created,aladin_users.activated,aladin_partners_account.pmode,aladin_partners_account.accountnmb,aladin_partners_account.bcode,aladin_partners_account.ibn,aladin_partners_account.mobile FROM aladin_partners_account  JOIN aladin_users ON aladin_partners_account.user_id=aladin_users.user_id `,function(err,result){
    if(err) throw err
      res.send({
        detail:result,
    });
  });

});

router.get('/partners', function(req, res, next) {

  /**
   * SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
FROM Orders
INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID;
   */

  db.query(`SELECT aladin_users.user_id,aladin_users.name,aladin_users.email,aladin_users.country,aladin_users.created,aladin_users.activated,aladin_partners_firms.sr FROM aladin_partners_firms  JOIN aladin_users ON aladin_partners_firms.user_id=aladin_users.user_id `,function(err,result){
    if(err) throw err
      res.send({
        partner:result,
    });
  });

});

router.post('/checkemail',(req,res)=>{
  let email = req.body.email;
  let sql =`SELECT name,email FROM aladin_users WHERE email like ?`;
  db.query(sql,['%'+email+'%'],(err,result)=>{
    if(err){
      res.send({
        error:err,
        status:false,
        message:"ERREUR BAD REQUEST"
      });
    }
    if(result.length>0){
      let user={
        id:result[0].user_id,
        status:true
      }
      res.send({user:user});
    }else{
      res.send({
        status:false,
        message:"Email introuvalbe"
      })
    }

  
  })
});


router.post("/register",(req,res)=>{
  var password = generator.generate({
    length:10,
    numbers: true
  });
 let date = order_date()
  const saltRounds = 10;
  let sql="INSERT INTO aladin_staff(name,role,email,password,phone,created_at) VALUES ?";

  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(password,salt, function(err, hash) {
      
      data=[ [
        req.body.name,
        req.body.role,
        req.body.email,
        hash,
        req.body.phone,
        date

      ]];

      db.query(sql,[data],(err,resultat)=>{
        if(err){
          res.send({
            status:false,
            message:"mauvais format des données ou email déjà utilisé",
            error:err
          })
          
        }
        let emaildata={
          password:password,
          role:req.body.role,
          name:req.body.name
        
        }

        let message={
          to:req.body.email,
          subject:"Creation de compte Aladin",
          html:createhtml(emaildata)
        };
        sendmail(message.to,message.subject,message.html);
        res.send(
          {status:false,
            message:"Vous avez crée un utilisateur",
            data:resultat
          });
      });

    });


});
 
});



router.post("/auth/register",(req,res)=>{
 let password=req.body.password
 let date = order_date()
  const saltRounds = 10;
  let sql="INSERT INTO aladin_users(name,email,password,phone,created,city,is_partner,whatssap,country) VALUES ?";
  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(password,salt, function(err, hash) {
      
      data=[ [
        req.body.name,
        req.body.email,
        hash,
        req.body.phone,
        date,
        req.body.city,
        req.body.is_partner,
        req.body.whatsapp,
        "CI"
      ]];

      db.query(sql,[data],(err,resultat)=>{
        if(err){
          res.send({
            error:err
          })
          
        }
        let emaildata={
       
          name:req.body.name
        
        }

        let message={
          to:req.body.email,
          subject:"Création de compte Aladin",
          html:createuserhtml(emaildata)
        };
        sendmail(message.to,message.subject,message.html);

        res.send({message:"Création de compte reussi!!!",data:resultat,role:req.body.is_partner});
      });

    });


});
 
});


router.post("/login",(req,res)=>{
  let email = req.body.email;
  let password=req.body.password;
 let sql=`SELECT * FROM aladin_staff WHERE email like ? LIMIT 1`;

 db.query(sql,['%' + email + '%'],(err,resultat,field)=>{
   if(err) {
   res.send({
     error:err,
     status:false
   })
   } 
   if(resultat.length>0){
    bcrypt.compare(password,resultat[0].password).then(function(result) {
      if(result==true){
        res.send({
          status:true,
          message:"Authentification reussi",
          user:{id:resultat[0].id,role:resultat[0].role}
          });
      }else{
        res.send({
          status:false,
          message:"Authentification echoué mot de passe introuvable",
        });
      }
    });
   }else{
    res.send({
      status:false,
      message:"Authentification echoué email introuvable",
      data:resultat
    });

   }
 });
});


router.post("/auth/login",(req,res)=>{
  let email = req.body.email;
  let password=req.body.password;
 let sql=`SELECT * FROM aladin_users WHERE email like ? LIMIT 1`;

 db.query(sql,['%' + email + '%'],(err,resultat,field)=>{
   if(err) {
   res.send({
     error:err
   })
   } 
   if(resultat.length>0){
    bcrypt.compare(password,resultat[0].password).then(function(result) {
      if(result==true){
        res.send({
          status:true,
          message:"Authentification reussi",
          user:{id:resultat[0].user_id,is_partner:resultat[0].is_partner}
          });
      }else{
        res.send({
          status:false,
          message:"Authentification echoué mot de passe introuvable",
        });
      }
    });
   }else{
    res.send({
      status:false,
      message:"Authentification echoué email introuvable",
    });
   }
 });
});


/**
 * 
 * 
 *     bcrypt.compare(password,el.password).then(function(result) {

         if(result==true){
           res.send({
             message:"Authentification reussi",
             user:field
           });
         }else{
           res.send({
             message:"Authentification echoué mot de passe introuvable",
           });
         }
        
     })
 * 
 * 
 * 
 */

router.put('/:id',(req,res)=>{
  let sql =`UPDATE aladin_users SET password=${req.body.password} WHERE user_id=${req.params.id}`
  db.query(sql,(err,result)=>{
    if(err) {
      res.send({
        eror:err,
        status:false,
        message:"Mauvaise requete"
      })

    }
    let message={
      to:req.body.email,
      subject:"Aladin password",
      html: pwdhtml({name:"Cher client"})
    };
    sendmail(message.to,message.subject,message.html);
    res.send({
      status:true,
      message:"Mot de passe réinitiasé",
      data:result});
  })
  
});

router.put('/staff/:id',(req,res)=>{
  let sql =`UPDATE aladin_staff SET password=${req.body.password} WHERE user_id=${req.params.id}`
  db.query(sql,(err,result)=>{
    if(err){
      res.send({
        error:err,
        status:false,
        message:"Mauvais format des données"
      })
    } 
    let message={
      to:req.body.email,
      subject:"Aladin password",
      html:"Votre mot de passe a été reinitialisé."
    };
    sendmail(message.to,message.subject,message.html);
    res.send({
      status:true,
      message:"succès",
      data:result});

  })
  
});




router.post("/cloths",(req,res)=>{
  const file = req.files.img;
  let sql=`INSERT INTO aladin_cloths(name_cloths,price,is_ordered,created_at,img,staff,comment,material,size,type_imp) VALUES ?`;
   let date = order_date()
file.mv("public/cloths/"+file.name,(err,resp)=>{
if(err) throw err
var data=[[req.body.name,req.body.price,req.body.is_ordered,date,"https://api.aladin.ci/cloths/"+file.name,req.body.staff,req.body.description,req.body.material,req.body.size,req.body.type_imp]]
 
db.query(sql,[data],(err,result)=>{
   if(err){
    res.send({
      error:err,
      status:false,
      massage:"Mauvais format des données ou champ nom reseignés"
    })
  }
  res.send({
    status:true,
    message:"Les données ont bien été enregistrées!",
    data:result
  })


})

});


});


router.post("/packs",(req,res)=>{
  const file = req.files.img;
  let sql=`INSERT INTO aladin_packaging(name_packaging,price,is_ordered,created_at,img,staff,comment,dim,material) VALUES ?`;
   let date = order_date()
file.mv("public/packs/"+file.name,(err,resp)=>{
if(err) throw err
var data=[[req.body.name,req.body.price,req.body.is_ordered,date,"https://api.aladin.ci/packs/"+file.name,req.body.staff,req.body.description,req.body.dim,req.body.material]]
 
db.query(sql,[data],(err,result)=>{
  if(err){
    res.send({
      error:err,
      status:false,
      massage:"Mauvais format des données ou champ nom reseignés"
    })
  }
  res.send({
    status:true,
    message:"Les données ont bien été enregistrées!",
    data:result
  })


})

});


});



router.post("/gadgets",(req,res)=>{
  const file = req.files.img;
  let sql=`INSERT INTO aladin_gadgets(name_gadget,price,is_ordered,created_at,img,staff,comment) VALUES ?`;
   let date = order_date()
file.mv("public/gadgets/"+file.name,(err,resp)=>{
if(err) throw err
var data=[[req.body.name,req.body.price,req.body.is_ordered,date,"https://api.aladin.ci/gadgets/"+file.name,req.body.staff,req.body.description]]
 
db.query(sql,[data],(err,result)=>{
  if(err){
    res.send({
      error:err,
      status:false,
      massage:"Mauvais format des données ou champ nom reseignés"
    })
  }
  res.send({
    status:true,
    message:"Les données ont bien été enregistrées!",
    data:result
  })
})

});


});




router.post("/disps",(req,res)=>{
  const file = req.files.img;
  let sql=`INSERT INTO aladin_displays(name_display,price,dim,is_ordered,created_at,img,staff,comment) VALUES ?`;
   let date = order_date()
file.mv("public/disps/"+file.name,(err,resp)=>{
if(err) throw err
var data=[[req.body.name,req.body.price,req.body.dim,req.body.is_ordered,date,"https://api.aladin.ci/disps/"+file.name,parseInt(req.body.staff),req.body.description]]
 
db.query(sql,[data],(err,result)=>{
  if(err){
    res.send({
      error:err,
      status:false,
      massage:"Mauvais format des données ou champ nom reseignés"
    })
  }
  res.send({
    status:true,
    message:"Les données ont bien été enregistrées!",
    data:result
  })

})

});


});







router.post("/prints",(req,res)=>{
  const file = req.files.img;
  let sql=`INSERT INTO aladin_printed(name_printed,price,gram,is_ordered,created_at,img,staff,comment,pellicule,volet) VALUES ?`;
   let date = order_date()
file.mv("public/prints/"+file.name,(err,resp)=>{
if(err) throw err
var data=[[req.body.name,req.body.price,req.body.gram,req.body.is_ordered,date,"https://api.aladin.ci/prints/"+file.name,req.body.staff,req.body.description,req.body.pellicule,req.body.volet]]
 
db.query(sql,[data],(err,result)=>{
  if(err){
    res.send({
      error:err,
      massage:"Mauvais format des données ou champ nom reseignés"
    })
  }
  res.send({
    status:true,
    message:"Les données ont bien été enregistrées!",
    data:result
  })

})

});


});


router.post('/disps/images',(req,res)=>{

  let filef=req.files.myfront
  let fileb=req.files.myback
  let filecf=req.files.mycfront
  let filecb = req.files.mycback
  let date= order_date()
  let sql=`INSERT INTO aladin_images(created_at,disp,front_side,back_side,customimg,custimg) VALUES ?`;
  data=[[date,req.body.disp,"https://api.aladin.ci/disps/"+filef.name,"https://api.aladin.ci/disps/"+fileb.name,"https://api.aladin.ci/disps/"+filecf.name,"https://api.aladin.ci/disps/"+filecb.name]];

  filef.mv("public/disps/"+filef.name,(err,resp)=>{

    if(err) throw err

    fileb.mv("public/disps/"+fileb.name,(err,rest)=>{

      if(err) throw err;
      filecf.mv("public/disps/"+filecf.name,(err,reqs)=>{
        if(err) throw err;
        filecb.mv("public/disps/"+filecb.name,(error,re)=>{
          if(error) throw error;

          db.query(sql,[data],(erreur,myresult)=>{
            if(erreur){
              res.send({
                error:erreur,
                status:false,
                message:"une erreur s'est produite verifiez le format des données "
              })
            }
            res.send({
              "message":"Les données ont bien étés enregistrés",
              data:myresult,
              status:true,
            })
          })

        })
      })

    })
  })
  

})


router.get('/aladin/staff',(req,res)=>{
  let sql ="SELECT name,email,phone,role FROM aladin_staff LIMIT 100";
  db.query(sql,(err,resp)=>{
    if(err){
      res.send({
        error:err,
        status:false
      })
    }
    if(resp.length>0){
      res.send({
        status:true,
        users:resp
      })
    }else{
      res.send({
        status:true,
        message:"aucun objets trouvé"
      })
    }


  })
})

router.post('/packs/images',(req,res)=>{

  let filef=req.files.myfront
  let fileb=req.files.myback
  let filecf=req.files.mycfront
  let filecb = req.files.mycback
  let date= order_date()
  let sql=`INSERT INTO aladin_images(created_at,pack,front_side,back_side,customimg,custimg) VALUES ?`;
  data=[[date,req.body.disp,"https://api.aladin.ci/packs/"+filef.name,"https://api.aladin.ci/packs/"+fileb.name,"https://api.aladin.ci/packs/"+filecf.name,"https://api.aladin.ci/packs/"+filecb.name]];

  filef.mv("public/packs/"+filef.name,(err,resp)=>{

    if(err) throw err

    fileb.mv("public/packs/"+fileb.name,(err,rest)=>{

      if(err) throw err;
      filecf.mv("public/packs/"+filecf.name,(err,reqs)=>{
        if(err) throw err;
        filecb.mv("public/packs/"+filecb.name,(error,re)=>{
          if(error) throw error;

          db.query(sql,[data],(erreur,myresult)=>{
            if(erreur){
              res.send({
                error:erreur,
                status:false,
                message:"une erreur s'est produite verifiez le format des données "
              })
            }
            res.send({
              message:"Les données ont bien étés enregistrés",
              data:myresult,
              status:true,
            })
          })

        })
      })

    })
  })
  

})

router.post('/cloths/images',(req,res)=>{

  let filef=req.files.myfront
  let fileb=req.files.myback
  let filecf=req.files.mycfront
  let filecb = req.files.mycback
  let date= order_date()
  let sql=`INSERT INTO aladin_images(created_at,cloth,front_side,back_side,customimg,custimg) VALUES ?`;
  data=[[date,req.body.disp,"https://api.aladin.ci/cloths/"+filef.name,"https://api.aladin.ci/cloths/"+fileb.name,"https://api.aladin.ci/cloths/"+filecf.name,"https://api.aladin.ci/cloths/"+filecb.name]];

  filef.mv("public/cloths/"+filef.name,(err,resp)=>{

    if(err) throw err

    fileb.mv("public/cloths/"+fileb.name,(err,rest)=>{

      if(err) throw err;
      filecf.mv("public/cloths/"+filecf.name,(err,reqs)=>{
        if(err) throw err;
        filecb.mv("public/cloths/"+filecb.name,(error,re)=>{
          if(error) throw error;

          db.query(sql,[data],(erreur,myresult)=>{
            if(erreur){
              res.send({
                error:erreur,
                status:false,
                message:"une erreur s'est produite verifiez le format des données "
              })
            }
            res.send({
              message:"Les données ont bien étés enregistrés",
              data:myresult,
              status:true,
            })
          })

        })
      })

    })
  })
  

})





router.post('/gadgets/images',(req,res)=>{

  let filef=req.files.myfront
  let fileb=req.files.myback
  let filecf=req.files.mycfront
  let filecb = req.files.mycback
  let date= order_date()
  let sql=`INSERT INTO aladin_images(created_at,gadget,front_side,back_side,customimg,custimg) VALUES ?`;
  data=[[date,req.body.disp,"https://api.aladin.ci/gadgets/"+filef.name,"https://api.aladin.ci/gadgets/"+fileb.name,"https://api.aladin.ci/gadgets/"+filecf.name,"https://api.aladin.ci/gadgets/"+filecb.name]];

  filef.mv("public/gadgets/"+filef.name,(err,resp)=>{

    if(err) throw err

    fileb.mv("public/gadgets/"+fileb.name,(err,rest)=>{

      if(err) throw err;
      filecf.mv("public/gadgets/"+filecf.name,(err,reqs)=>{
        if(err) throw err;
        filecb.mv("public/gadgets/"+filecb.name,(error,re)=>{
          if(error) throw error;

          db.query(sql,[data],(erreur,myresult)=>{
            if(erreur){
              res.send({
                error:erreur,
                status:false,
                message:"une erreur s'est produite verifiez le format des données "
              })
            }
            res.send({
              "message":"Les données ont bien étés enregistrés",
              data:myresult,
              status:true,
            })
          })

        })
      })

    })
  })
  

})





router.post('/prints/images',(req,res)=>{

  let filef=req.files.myfront
  let fileb=req.files.myback
  let filecf=req.files.mycfront
  let filecb = req.files.mycback
  let date= order_date()
  let sql=`INSERT INTO aladin_images(created_at,print,front_side,back_side,customimg,custimg) VALUES ?`;
  data=[[date,req.body.disp,"https://api.aladin.ci/prints/"+filef.name,"https://api.aladin.ci/prints/"+fileb.name,"https://api.aladin.ci/prints/"+filecf.name,"https://api.aladin.ci/prints/"+filecb.name]];

  filef.mv("public/prints/"+filef.name,(err,resp)=>{

    if(err) throw err

    fileb.mv("public/prints/"+fileb.name,(err,rest)=>{

      if(err) throw err;
      filecf.mv("public/prints/"+filecf.name,(err,reqs)=>{
        if(err) throw err;
        filecb.mv("public/prints/"+filecb.name,(error,re)=>{
          if(error) throw error;

          db.query(sql,[data],(erreur,myresult)=>{
            if(erreur){
              res.send({
                error:erreur,
                status:false,
                message:"une erreur s'est produite verifiez le format des données "
              })
            }
            res.send({
              "message":"Les données ont bien étés enregistrés",
              data:myresult,
              status:true,
            })
          })

        })
      })

    })
  })
  
})



router.get('/texts/:id',(req,res)=>{
  let sql =`SELECT * FROM aladin_texts WHERE image= ${req.params.id}`;
  db.query(sql,(err,result)=>{
    if(err){
      res.send({
        status:false,
        error:err,
        message:"erreur la requete a echoué"
      })
    }
    res.send({
      status:true,
      data:result,
      message:"success"
    })
  })
})
status:true



router.post('/texts',(req,res,next)=>{
  let data= [[
    req.body.font,
    (req.body.top).toFixed(2),
    (req.body.left).toFixed(2),
    req.body.text,
    req.body.fill,
    req.body.image,
    req.body.is_back,

  ]];
  let sql ="INSERT INTO aladin_texts(fontfamily,y_value,x_value,text,fill,image,is_back) VALUES ?"

  db.query(sql,[data],(err,re)=>{
    if(err){
      res.send({
        status:false,
        message:"mauvaise requete",
        error:err
      })
    }
    res.send({
      status:true,
      data:re,
      message:"text enregistré"
    })
  })
})








module.exports = router;
