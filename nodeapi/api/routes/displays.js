var express = require('express');
var router = express.Router();
var db = require('../config/db')
const sendmail=require('../config/mail/notify')

/* Post uploaded file. */

router.post('/', function(req, res, next){
    const file = req.files.img;
    console.log(file,req.body);
    file.mv("public/disps/"+file.name,(err,resp)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_displays(name_display,price,dim,gram,img,user_id,comment) VALUES ?";
            var user=parseInt(req.body.user_id)

            var value=[
                [req.body.name_display,req.body.price,req.body.dim,req.body.gram,"https://api.aladin.ci/disps/"+file.name,user,req.body.comment]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    throw err
                }else{
                res.send({
                    message:"Vetement ajouté",
                    resp:result
                });
                }
            });
       }
    });
  
});

/**
 * get all uploaded cloths
 */
router.get('/',(req,res,next)=>{
sql = `SELECT * FROM aladin_displays WHERE is_ordered=${1}`;
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{
    res.send(result);
}
})
});


router.get('/:id',(req,res)=>{
    if(req.params.id){
        let sql=`SELECT aladin_displays.disp_id,aladin_displays.price,aladin_displays.dim,aladin_displays.is_ordered,aladin_displays.staff,aladin_displays.user_id,aladin_displays.comment,aladin_displays.created_at,aladin_displays.name_display,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back FROM aladin_images
        JOIN aladin_displays ON aladin_images.disp=aladin_displays.disp_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_displays.disp_id=${req.params.id}`;
        db.query(sql,(err,result)=>{
            if(err){
           res.send(err)
            } 
            res.send(result);
        })
    }
})







router.put('/:id',(req,res)=>{
    let sql =`UPDATE aladin_displays SET is_ordered=${req.body.is_ordered} WHERE disp_id=${req.params.id}`
    db.query(sql,(err,result)=>{
      if(err) throw err;
      let newsql=`SELECT user_id FROM aladin_displays WHERE disp_id=${req.params.id}`;
   
     db.query(newsql,(err,re)=>{
         if(err) throw err;
         let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;
         db.query(ql,(err,r)=>{
             if(err) throw err;
  
             let message={
        to:r[0].email,
        subject:"Activation de compte Aladin",
        html:"Votre produit a été approuvé."
      };
      sendmail(message.to,message.subject,message.html);
            res.send({
                message:"vous avez approuvé le produit",
                success:true
            });
         })
     });

    })
    
  });


module.exports = router;
